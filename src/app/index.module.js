(function() {
  'use strict';

  angular
    .module('setmassageFrontend', ['ngAnimate', 'ngCookies', 'ngTouch', 'ngSanitize', 'ngMessages', 'ngAria', 'ui.router', 'ui.bootstrap', 'toastr', 'angular-jwt', 'ngStorage', 'mm.acl']);

})();
