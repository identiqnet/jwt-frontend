/* global  moment:false */
(function () {
    'use strict';

    angular
        .module('setmassageFrontend')
        .constant('moment', moment)
        .constant('API_URL', 'http://localhost/~identiq/setmassage/setmassage-bend/web')
        .constant('REFRESH_TOKEN', 'setmassage')
        .constant('ROLE_ADMIN', 'ROLE_ADMIN')
        .constant('ROLES', {
            ROLE_VISITOR: ['login'],
            ROLE_USER: ['login', 'logout', 'view_content'],
            ROLE_ADMIN: ['login', 'logout', 'view_content', 'manage_content'],
            ROLE_SUPER_ADMIN: ['login', 'logout', 'view_content', 'manage_content']
        });

})();
