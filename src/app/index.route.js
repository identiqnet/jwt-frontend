(function () {
    'use strict';

    angular
        .module('setmassageFrontend')
        .config(routerConfig);

    /** @ngInject */
    function routerConfig($stateProvider, $urlRouterProvider, $httpProvider) {

        $httpProvider.interceptors.push('authInterceptor');

        $stateProvider
            .state('login', {
                url: '/login',
                templateUrl: 'app/login/login.html',
                controller: 'LoginController',
                controllerAs: 'vm'
            })


            .state('app', {
                url: '/app',
                abstract: true,
                template: '<ui-view/>',
                resolve: {
                    logged: loggedRoute
                }
            })

            .state('app.users', {
                url: '/users',
                templateUrl: 'app/users/users.html',
                controller: 'UsersController',
                controllerAs: 'vm',
                resolve: {
                    admin: adminRoute
                }
            });

        $urlRouterProvider.otherwise('/app/users');

        /** @ngInject */
        function loggedRoute(auth, $state) {

            return auth.isLogged().catch(function() {
                $state.go('login');
            });

        }

        /** @ngInject */
        function adminRoute(auth, $state) {

            return auth.isAdmin().catch(function() {
                $state.go('login');
            });
        }
    }

})();
