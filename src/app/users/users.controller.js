(function() {
    'use strict';

    angular
        .module('setmassageFrontend')
        .controller('UsersController', UsersController);

    /** @ngInject */
    function UsersController(users, AclService) {
        var vm = this;

        vm.users = [];
        vm.can = AclService.can;

        activate();

        function activate() {

            users.all().then(usersComplete);

            function usersComplete(users) {
                vm.users = users;
            }
        }



    }
})();
