(function () {
    'use strict';

    angular
        .module('setmassageFrontend')
        .service('users', users);

    /** @ngInject */
    function users($http, $q, API_URL, $log, toastr) {

        var service = {
            all: all,
            one: one,
            create: create,
            edit: edit,
            remove: remove
        };

        return service;

        function all() {

            var deferred = $q.defer();

            $http.get(API_URL + '/api/v1/users.json').then(allComplete).catch(allError);

            function allComplete(res) {
                $log.debug('usersComplete', res.data);
                deferred.resolve(res.data);
            }

            function allError(err) {
                toastr.error(angular.toJson(err.data.message), 'Error!');
                deferred.reject(err);
            }

            return deferred.promise;
        }

        function one(userId) {

            var deferred = $q.defer();

            $http.get(API_URL + '/api/v1/users/{userId}.json'.replace('{userId}', userId))
                .then(oneComplete).catch(oneError);

            function oneComplete(res) {
                $log.debug('oneComplete', res.data);
                deferred.resolve(res.data);
            }

            function oneError(err) {
                toastr.error(angular.toJson(err.data.message), 'Error!');
                deferred.reject(err);
            }

            return deferred.promise;
        }

        function create(user) {

            var deferred = $q.defer();

            $http.post(API_URL + '/api/v1/users.json', {
                username: user.username,
                password: user.password
            }).then(createComplete).catch(createError);

            function createComplete(res) {
                $log.debug('createComplete', res.data);
                deferred.resolve(res.data);
            }

            function createError(err) {
                toastr.error(angular.toJson(err.data.message), 'Error!');
                deferred.reject(err);
            }

            return deferred.promise;
        }

        function edit(userId, user) {

            var deferred = $q.defer();

            $http.put(API_URL + '/api/v1/users/{userId}.json'.replace('{userId}', userId), {
                username: user.username,
                password: user.password
            }).then(putComplete).catch(putError);

            function putComplete(res) {
                $log.debug('putComplete', res.data);
                deferred.resolve(res.data);
            }

            function putError(err) {
                toastr.error(angular.toJson(err.data.message), 'Error!');
                deferred.reject(err);
            }

            return deferred.promise;
        }

        function remove(userId) {

            var deferred = $q.defer();

            $http.delete(API_URL + '/api/v1/users/{userId}.json'.replace('{userId}', userId))
                .then(removeComplete).catch(removeError);

            function removeComplete(res) {
                $log.debug('removeComplete', res.data);
                toastr.success('User successfully removed!');

                deferred.resolve(res.data);
            }

            function removeError(err) {
                toastr.error(angular.toJson(err.data.message), 'Error!');
                deferred.reject(err);
            }

            return deferred.promise;
        }
    }

})();
