(function () {
    'use strict';

    angular
        .module('setmassageFrontend')
        .run(runBlock);

    /** @ngInject */
    function runBlock($log, AclService, ROLES) {

        $log.debug('runBlock end');

        if (!AclService.resume()) {
            AclService.attachRole('ROLE_VISITOR');
            AclService.setAbilities(ROLES);
        }
    }

})();
