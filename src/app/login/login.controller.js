(function() {
  'use strict';

  angular
    .module('setmassageFrontend')
    .controller('LoginController', LoginController);

  /** @ngInject */
  function LoginController(auth, $state) {
    var vm = this;

    vm.login = login;

    function login(username, password) {
      auth.login(username, password).then(loginComplete);

      function loginComplete() {
        $state.go('app.users');
      }
    }

  }
})();
