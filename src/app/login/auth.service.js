(function () {
    'use strict';

    angular
        .module('setmassageFrontend')
        .service('auth', auth);

    /** @ngInject */
    function auth($http, $q, API_URL, REFRESH_TOKEN, $log, $localStorage, toastr, ROLE_ADMIN, AclService) {

        var service = {
            login: login,
            isLogged: isLogged,
            isAdmin: isAdmin,
            logged: logged,
            logout: logout
        };

        return service;

        function login(username, password) {

            var deferred = $q.defer();

            $http.post(API_URL + '/get_token', {
                username: username,
                password: password,
                refresh_token_key: REFRESH_TOKEN
            }, {skipAuthorization: true}).then(loginComplete).catch(loginError);

            function loginComplete(res) {
                $log.debug('loginComplete', res.data);
                setRole(res.data.data.roles);
                setToken(res.data);
                deferred.resolve(res);
            }

            function loginError(err) {
                toastr.error(angular.toJson(err.data.message), 'Error!');
                deferred.reject(err);
            }

            function setRole(roles) {
                angular.forEach(roles, addRole);

                function addRole(role) {
                    AclService.attachRole(role);
                }
            }

            function setToken(token) {
                $localStorage.token = token.token;
                $localStorage.refresh_token = token.refresh_token;
            }

            return deferred.promise;
        }

        function isLogged() {
            var deferred = $q.defer();

            if(!$localStorage.token || !$localStorage.refresh_token) {
                deferred.reject();
            } else {
                deferred.resolve();
            }

            return deferred.promise;
        }

        function logged() {
            return !$localStorage.token || !$localStorage.refresh_token;
        }

        function logout() {

            delete $localStorage.token;
            delete $localStorage.refresh_token;

        }

        function isAdmin() {
            var deferred = $q.defer();

            $log.debug('isAdmin', AclService.hasRole(ROLE_ADMIN));

            if(AclService.hasRole(ROLE_ADMIN)) {
                deferred.resolve();
            } else {
                deferred.reject();
            }

            return deferred.promise;
        }

    }

})();
