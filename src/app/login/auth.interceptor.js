(function () {
    'use strict';

    angular
        .module('setmassageFrontend')
        .service('authInterceptor', authInterceptor);

    /** @ngInject */
    function authInterceptor($injector, $q, $localStorage, API_URL, jwtHelper) {

        var service = {
            request: request,
            responseError: responseError
        };
        return service;

        function request(config) {
            config = config || {};
            if ($localStorage.token && jwtHelper.isTokenExpired($localStorage.token)) {

                return $injector.get("$http")({
                    url: API_URL + '/refresh_token',
                    skipAuthorization: true,
                    method: 'POST',
                    refresh_token: $localStorage.refresh_token
                }).then(function (response) {
                    $localStorage.token = response.data.token;
                    $localStorage.refresh_token = response.data.refresh_token;
                    return response.data.token;
                });
            } else {
                config.headers.Authorization = 'Bearer ' + $localStorage.token;
            }
            return config;
        }

        function responseError(err) {
            if (err.status === 401 && err.config.url.indexOf('login') < 0) {
                $injector.get("$state").go('login');
                return $q.defer().promise;
            }
            return $q.reject(err);
        }
    }
})();
